package com.vehicle.manager.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.vehicle.manager.model.ColorVo;
import com.vehicle.manager.model.VariantColorVo;
import com.vehicle.manager.model.VariantVo;
import com.vehicle.manager.model.VehicleVo;
import com.vehicle.manager.vto.VehicleVto;

@Component
public interface VehicleManagerDao {
	
	public List<VehicleVo> getFullVehicleList();
	//public List<VariantColorVo> getColorsForVariantId(Long variantId);
	
	public VehicleVo insertVehicle(VehicleVo vehicleVo); 
	public VariantVo insertUpdateVariant(VariantVo variantVo);
	public ColorVo insertUpdateColor(ColorVo colorVo);
	public VariantColorVo insertUpdateVariantColor(VariantColorVo variantColorVo);
	public void deleteVehicle(Long vehicleId);
	public void deleteVariant(Long variantId);
	public void deleteColor(Long colorCode);
	public void deleteVariantColor(Long variantId, Integer colorCode);


}
