package com.vehicle.manager.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicle.manager.dao.VehicleManagerDao;
import com.vehicle.manager.model.ColorVo;
import com.vehicle.manager.model.VariantColorVo;
import com.vehicle.manager.model.VehicleVo;
import com.vehicle.manager.util.VehicleVtoMapper;
import com.vehicle.manager.vto.VehicleVto;

@Service
public class VehicleServiceImpl implements VehicleService {
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	VehicleManagerDao vehicleManagerDao; 
	
	@Autowired
	VehicleVtoMapper vehicleVtoMapper; 
	
	public List<VehicleVto> getFullVehicleList() {
		log.info("Making a DAO call for getting full Vehicle List from Vehicle Service");
		List<VehicleVo> vehicleVoList = vehicleManagerDao.getFullVehicleList();
		
		List<VehicleVto> vehicleVtoList = new ArrayList<VehicleVto>();

		for (VehicleVo vehicleVo : vehicleVoList) {

			vehicleVtoList.add(vehicleVtoMapper.mapVehicleVoToVehicleVto(vehicleVo));

		}
		
		log.info("Getting Vehicle list in VTO object is done on service");
		return vehicleVtoList ;
	}
	

	
	public boolean insertVehicle(VehicleVto vehicleVto)
	{
		log.info("Inserting VehicleVto {}", vehicleVto.toString() );
		VehicleVo vehicleVo = vehicleVtoMapper.mapVehicleVtoToVehicleVo(vehicleVto);
		VehicleVo insertedVo = vehicleManagerDao.insertVehicle(vehicleVo);
		
		
		log.info("After inserted of VO is {}", insertedVo);
		
		if(!insertedVo.getVehicleId().equals(null))
		{
			return true; 
		}
		else
		{
			return false; 
		}
		
		
	}



	@Override
	public ColorVo insertUpdateColor(ColorVo colorVo) {
		log.info("Inserting/Updating Color Vo {}", colorVo);
		return vehicleManagerDao.insertUpdateColor(colorVo);
	}



	@Override
	public VariantColorVo insertUpdateVariantColor(VariantColorVo variantColorVo) {
		log.info("Inserting/Updating Variantcolor Vo {}", variantColorVo);
		return vehicleManagerDao.insertUpdateVariantColor(variantColorVo);
	}



	@Override
	public void deleteVehicle(Long vehicleId) {
		log.info("Deleting the Vehicle By Vehicle ID {}", vehicleId);
		vehicleManagerDao.deleteVehicle(vehicleId);
		
	}



	@Override
	public void deleteVariant(Long variantId) {
		log.info("Deleting the Variant By Variant ID {}", variantId);
		vehicleManagerDao.deleteVariant(variantId);
	}



	@Override
	public void deleteColor(Long colorCode) {
		log.info("Deleting the Color By ColorCode {}", colorCode);
		vehicleManagerDao.deleteColor(colorCode);
	}



	@Override
	public void deleteVariantColor(Long variantId, Integer colorCode) {
		log.info("Deleting the VariantColor  By variant ID {} and Color Code ", variantId, colorCode);
		vehicleManagerDao.deleteVariantColor(variantId, colorCode);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
