package com.vehicle.manager.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.vehicle.manager.model.ColorVo;
import com.vehicle.manager.model.VariantColorVo;
import com.vehicle.manager.vto.VehicleVto;

@Service
public interface VehicleService {

	public List<VehicleVto> getFullVehicleList() ;
	public boolean insertVehicle(VehicleVto vehicleVto);
	public ColorVo insertUpdateColor(ColorVo colorVo);
	public VariantColorVo insertUpdateVariantColor(VariantColorVo variantColorVo);
	public void deleteVehicle(Long vehicleId);
	public void deleteVariant(Long variantId);
	public void deleteColor(Long colorCode);
	public void deleteVariantColor(Long variantId, Integer colorCode);
}
