package com.vehicle.manager.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vehicle.manager.model.VariantVo;
@Repository
public interface VariantRepo extends JpaRepository<VariantVo, Long>{
	@Transactional
	@Modifying
	@Query ("delete from VariantVo var where var.vehicleId= :vehicleId")
	public void deleteVariantByVehicleId(@Param("vehicleId") Long vehicleId); 
	
	@Query("select var.variantId from VariantVo var where var.vehicleId= :vehicleId")
	public List<Long> findAllIdByVechicleId(@Param("vehicleId") Long vehicleId);

}
