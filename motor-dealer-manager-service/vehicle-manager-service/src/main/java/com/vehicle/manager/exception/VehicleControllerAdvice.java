package com.vehicle.manager.exception;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.security.auth.login.AccountNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class VehicleControllerAdvice extends ResponseEntityExceptionHandler  {

	Logger log = LoggerFactory.getLogger(getClass());
	
	
	 @ResponseStatus(HttpStatus.NOT_FOUND) // 404
	  @ExceptionHandler(AccountNotFoundException.class)
	  public ResponseEntity<Object> handleNotFound(AccountNotFoundException ex) {
		 Map<String, Object> body = new LinkedHashMap<>();
		    body.put("timestamp", LocalDateTime.now());
		    body.put("message", "Not Found");
		
		    return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
	  }
	 
	  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 500
	  @ExceptionHandler(Exception.class)
	  public ResponseEntity<Object> handleGeneralError(Exception ex) {
			Map<String, Object> body = new LinkedHashMap<>();
		    body.put("timestamp", LocalDateTime.now());
		    body.put("message", "Something gone wrong in service");
		
		    return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
	  }
	
	
}
