package com.vehicle.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class VehicleManagerServiceApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(VehicleManagerServiceApplication.class, args);
	}

	

}
