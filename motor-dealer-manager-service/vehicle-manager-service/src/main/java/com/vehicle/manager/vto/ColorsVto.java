package com.vehicle.manager.vto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public  class ColorsVto {
	private Long colorCode;
	private String colorName;
	private Integer stock =0;
	

	
}