package com.vehicle.manager.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@Entity
@Table(name ="variant")
public class VariantVo {
	
	@Id
	@Column(name="variant_id")
	private Long variantId;
	private String variantName;
	private String transmission;
	private String fuelType;
	private String engineModel; 
	@Column(name="vehicle_id")
	private long vehicleId;
	
	
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "vehicle_id", nullable = false,insertable = false,updatable = false)
    private VehicleVo vehicle;
	
	
	@OneToMany (fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "variant")
	private List<VariantColorVo> colorsList; 
	
	

	public VariantVo(Long variantId, String variantName, String transmission, String fuelType, String engineModel,
			long vehicleId) {
		super();
		this.variantId = variantId;
		this.variantName = variantName;
		this.transmission = transmission;
		this.fuelType = fuelType;
		this.engineModel = engineModel;
		this.vehicleId = vehicleId;
	}

	@Override
	public String toString() {
		return "VariantVo [variantId=" + variantId + ", variantName=" + variantName + ", transmission=" + transmission
				+ ", fuelType=" + fuelType + ", engineModel=" + engineModel + "]";
	}
	
	

	
	
	
}
