package com.vehicle.manager.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@AllArgsConstructor @NoArgsConstructor
public class VariantColorId implements Serializable{
	
	
	@Column(name="variant_id")
	private Long variantId; 
	
	
	@Column(name="color_code")
	private Long colorCode; 
	

}
