package com.vehicle.manager.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.vehicle.manager.model.ColorVo;
import com.vehicle.manager.model.VariantColorId;
import com.vehicle.manager.model.VariantColorVo;
import com.vehicle.manager.model.VariantVo;
import com.vehicle.manager.model.VehicleVo;
import com.vehicle.manager.vto.ColorsVto;
import com.vehicle.manager.vto.VariantVto;
import com.vehicle.manager.vto.VehicleVto;

@Component
public class VehicleVtoMapper {
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	private VehicleVto vehicleVto; 
	
	/** We are getting VehicleVo from Database and have to 
	 * map that to a single object VehicleVTO to produce json **/
	public  VehicleVto mapVehicleVoToVehicleVto(VehicleVo vehicleVo)
	{
		log.info("Mapping VehicleVo to VehicleVto started");
		VehicleVto vehicleVto = new VehicleVto();
		vehicleVto.setVehicleId(vehicleVo.getVehicleId());
		vehicleVto.setVehicleName(vehicleVo.getVehicleName());
		vehicleVto.setModel(vehicleVo.getModel());
		vehicleVto.setMakeYear(vehicleVo.getMakeYear());
		
		List<VariantVto> variantVtoList = new ArrayList<VariantVto>();
		int vehicleStock = vehicleVto.getVehicleTotalAvailableStock();
		
		for(VariantVo variant : vehicleVo.getVariantList())
		{
			VariantVto variantVto = mapVariantVoToVariantVto(variant);
			variantVtoList.add(variantVto);
			vehicleStock = vehicleStock + variantVto.getVariantTotalAvailableStock();
			
		}
		//vehicleStock = vehicleStock+ variantVto.getVariantTotalAvailableStock();
		vehicleVto.setVehicleTotalAvailableStock(vehicleStock);
		vehicleVto.setVariants(variantVtoList);
		
		log.info("Mapping VehicleVo to VehicleVto completed");
		
		return vehicleVto; 
	}
	
	/** Mapping variantVo to VariantVto in vehicleVto **/
	
	public  VariantVto mapVariantVoToVariantVto(VariantVo variant)
	{
		log.info("Mapping VariantVo  to VariantVTO started");
		VariantVto vehicleVariantVto =new VariantVto();
		vehicleVariantVto.setVariantId(variant.getVariantId());
		vehicleVariantVto.setVariantName(variant.getVariantName());
		vehicleVariantVto.setEngineModel(variant.getEngineModel());
		vehicleVariantVto.setFuelType(variant.getFuelType());
		vehicleVariantVto.setTransmission(variant.getTransmission());
		
		vehicleVariantVto =mapColorsListToVariantVto(vehicleVariantVto,variant.getColorsList());
		
		log.info("Mapping VariantVo  to VariantVTO completed");
		return vehicleVariantVto;
	}
	
	/** Getting VariantColorVo from variant_color table and add to colorsVto list on each VariantVto**/
	public  VariantVto mapColorsListToVariantVto(VariantVto variantVto, List<VariantColorVo> variantColorVo)
	{
		log.info("Mapping ColorsList into VariantVto started");
		List<ColorsVto> vehVarColorsVtoList = new ArrayList<ColorsVto>();
		int variantStock  = variantVto.getVariantTotalAvailableStock() ;
		for(VariantColorVo varColorVo: variantColorVo)
		{
			ColorsVto vehVarColorsVto =new ColorsVto();
			vehVarColorsVto.setColorCode(varColorVo.getColor().getColorCode());
			vehVarColorsVto.setColorName(varColorVo.getColor().getColorName());
			vehVarColorsVto.setStock(varColorVo.getAvailableStock());
			variantStock = variantStock+ vehVarColorsVto.getStock();
			
			vehVarColorsVtoList.add(vehVarColorsVto);
		}
		variantVto.setVariantTotalAvailableStock(variantStock);
		variantVto.setColors(vehVarColorsVtoList);
		log.info("Mapping ColorsList into VariantVto completed");
		
		return variantVto; 
	}
	
	
	/** **/
	public  VehicleVo mapVehicleVtoToVehicleVo(VehicleVto vehicleVto)
	{
		log.info("Mapping VehicleVto into VehicleVo started");
		VehicleVo vehicleVo = new VehicleVo();
		vehicleVo.setVehicleId(vehicleVto.getVehicleId());
		vehicleVo.setVehicleName(vehicleVto.getVehicleName());
		vehicleVo.setModel(vehicleVto.getModel());
		vehicleVo.setMakeYear(vehicleVto.getMakeYear());
		
		List<VariantVo> variantVoList = new ArrayList<VariantVo>();
		for(VariantVto variantVto : vehicleVto.getVariants())
		{
			variantVoList.add(mapVariantVtoToVariantVo(vehicleVo.getVehicleId(),variantVto));
			
		}
		vehicleVo.setVariantList(variantVoList);
		log.info("Mapping VehicleVto into VehicleVo completed");
		
		return vehicleVo;
	}
	
	
	public  VariantVo mapVariantVtoToVariantVo(Long vehicleId, VariantVto variantVto)
	{
		log.info("Mapping VariantVto into VariantVo started");
		VariantVo variantVo = new VariantVo(); 
		variantVo.setVariantId(variantVto.getVariantId());
		variantVo.setVariantName(variantVto.getVariantName());
		variantVo.setEngineModel(variantVto.getEngineModel());
		variantVo.setFuelType(variantVto.getFuelType());
		variantVo.setTransmission(variantVto.getTransmission());
		variantVo.setVehicleId(vehicleId);
		
		List<VariantColorVo> variantColorVoList = new ArrayList<VariantColorVo>();
		for (ColorsVto colorsVto: variantVto.getColors())
		{
			
			VariantColorVo variantColorVo = new VariantColorVo();
			
			ColorVo colorVo = new ColorVo();
			colorVo.setColorCode(colorsVto.getColorCode());
			colorVo.setColorName(colorsVto.getColorName());
			
			VariantColorId variantColorId = new VariantColorId();
			variantColorId.setColorCode(colorsVto.getColorCode());
			variantColorId.setVariantId(variantVto.getVariantId());

			variantColorVo.setId(variantColorId);
			variantColorVo.setColor(colorVo);
			variantColorVo.setVariant(variantVo);
			variantColorVo.setAvailableStock(colorsVto.getStock());
			
			variantColorVoList.add(variantColorVo);
		}
		
		variantVo.setColorsList(variantColorVoList);
		
		log.info("Mapping VariantVto into VariantVo completed");
		return variantVo;
		
	}
	
	public ColorVo mapColorsVtoToColorsVo(ColorsVto colorsVto)
	{
		log.info("Mapping ColorsVto into ColorsVo started");
		ColorVo colorVo = new ColorVo() ;
		colorVo.setColorCode(null);
		colorVo.setColorName(null);
		log.info("Mapping ColorsVto into ColorsVo completed");
		return colorVo;
	}
	
	
	

}
