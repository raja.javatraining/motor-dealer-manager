package com.vehicle.manager.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @NoArgsConstructor
@Table(name="vehicle")
public class VehicleVo {
	@Id
	@Column(name="vehicle_id")
	private Long vehicleId; 
	private String vehicleName; 
	private String model;
	private int makeYear;
	//private Integer availableCount; 
	//private boolean inStock; 

	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "vehicle" )
	List<VariantVo> variantList ;

	public VehicleVo(Long vehicleId, String vehicleName, String model, int makeYear) {
		super();
		this.vehicleId = vehicleId;
		this.vehicleName = vehicleName;
		this.model = model;
		this.makeYear = makeYear;
	}

	
	
	
	

}
