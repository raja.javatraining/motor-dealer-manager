package com.vehicle.manager.vto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Required;

import com.vehicle.manager.model.VariantVo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VehicleVto implements Serializable {

	@NotNull
	private Long vehicleId;
	
	private String vehicleName;
	private String model;
	private int makeYear;
	private Integer vehicleTotalAvailableStock = 0;
	private List<VariantVto> variants = new ArrayList<VariantVto>();
	



}
