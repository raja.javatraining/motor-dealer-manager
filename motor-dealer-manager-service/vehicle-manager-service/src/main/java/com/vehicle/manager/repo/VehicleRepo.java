package com.vehicle.manager.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vehicle.manager.model.VehicleVo;

@Repository
public interface VehicleRepo extends JpaRepository<VehicleVo, Long> {
	
	//insert into vehicle_vo values ( 12, 'Hyundai', 'Black', '999','Petrol','Sedan', '8.66', 'Manual', 'China', 'Venue');

}
