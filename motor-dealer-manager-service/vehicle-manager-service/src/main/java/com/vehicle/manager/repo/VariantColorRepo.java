package com.vehicle.manager.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vehicle.manager.model.VariantColorId;
import com.vehicle.manager.model.VariantColorVo;

@Repository
public interface VariantColorRepo extends JpaRepository<VariantColorVo, VariantColorId> {

//	@Query ("select vc from VariantColorVo vc where vc.variantId= :variantId")
//	public List<VariantColorVo> fetchColorsForVariantId(@Param(value = "variantId") Long variantId); 
//	
	 @Transactional
	  @Modifying
	@Query ("delete from VariantColorVo vc where vc.variantId= :variantId and vc.colorCode= :colorCode")
	public void deleteByVariantAndColor(@Param(value = "variantId") Long variantId, @Param(value = "colorCode") Integer colorCode);
	
	 @Transactional
	  @Modifying
	@Query ("delete from VariantColorVo vc where vc.variantId= :variantId ")
	public void deleteByVariantId(@Param("variantId") Long variantId);
	
	
}
