package com.vehicle.manager.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="variant_colors")
@Getter @Setter @NoArgsConstructor
//@IdClass(VariantColorId.class)
public class VariantColorVo {
	
	@EmbeddedId
	private VariantColorId id; 
	
	@Column(name="variant_id", insertable = false, updatable = false)
	private Long variantId; 
	
	@Column (name="color_code",insertable = false, updatable = false)
	private Integer colorCode; 


	@ManyToOne(fetch = FetchType.EAGER ,cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@MapsId("variant_id")
	@JoinColumn(name="variant_id",referencedColumnName = "variant_id", nullable = false,insertable = false, updatable = false)
	private VariantVo variant; 
	
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@MapsId("color_code")
	@JoinColumn(name="color_code",referencedColumnName = "color_code", nullable = false, insertable = false, updatable = false)
	private ColorVo color;
	
	

	@Column(name="available_stock")
	private Integer availableStock; 
	
	public VariantColorVo( Integer availableStock, VariantVo variant, ColorVo color) {
		super();
		this.id = new VariantColorId(variant.getVariantId(),color.getColorCode());
		this.availableStock = availableStock;
//		this.variant = variant;
//		this.color = color;
	}
	
	
	

}
