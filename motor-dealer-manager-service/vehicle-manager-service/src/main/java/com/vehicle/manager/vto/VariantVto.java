package com.vehicle.manager.vto;

import java.util.ArrayList;
import java.util.List;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public  class VariantVto {
	private Long variantId;
	private String variantName;
	private String transmission;
	private String fuelType;
	private String engineModel;
	private Integer variantTotalAvailableStock = 0;
	private List<ColorsVto> colors = new ArrayList<ColorsVto>();
	


}