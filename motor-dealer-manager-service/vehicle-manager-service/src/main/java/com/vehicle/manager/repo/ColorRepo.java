package com.vehicle.manager.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vehicle.manager.model.ColorVo;
@Repository
public interface ColorRepo extends JpaRepository<ColorVo, Long>{

}
