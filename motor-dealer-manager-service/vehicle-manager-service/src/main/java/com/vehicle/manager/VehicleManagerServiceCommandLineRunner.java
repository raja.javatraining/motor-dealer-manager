package com.vehicle.manager;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.vehicle.manager.dao.VehicleManagerDao;
import com.vehicle.manager.model.ColorVo;
import com.vehicle.manager.model.VariantColorVo;
import com.vehicle.manager.model.VariantVo;
import com.vehicle.manager.model.VehicleVo;
import com.vehicle.manager.repo.ColorRepo;
import com.vehicle.manager.repo.VariantColorRepo;
import com.vehicle.manager.repo.VariantRepo;
import com.vehicle.manager.repo.VehicleRepo;

@Component
public class VehicleManagerServiceCommandLineRunner implements CommandLineRunner {
	
	@Autowired
	VehicleManagerDao vehicleManagerDao; 
	
	@Autowired 
	VariantRepo variantRepo;
	
	@Autowired 
	ColorRepo colorRepo;
	
	@Autowired
	VariantColorRepo  variantColorRepo;
	
	@Autowired
	VehicleRepo vehicleRepo;
	
	Logger log = LoggerFactory.getLogger(getClass());
	

	public void insertVehicle() throws Exception {
		
		
		log.info("Insert in Vehicle datbase started");
		
		VehicleVo vehicle= new VehicleVo(20200101l,"Suzuki","Ciaz",2021);
		vehicleRepo.save(vehicle);
		
		
		VariantVo variant1= new VariantVo(2020010101l,"VXI","7-Speed DCT","petrol", "1.0l Turbo", 20200101l);
		VariantVo variant2= new VariantVo(2020010102l,"LXI","7-Speed DCT","petrol", "1.0l Turbo", 20200101l);
		VariantVo variant3= new VariantVo(2020010103l,"ZXI","7-Speed DCT","petrol", "1.0l Turbo", 20200101l);
		
		variantRepo.saveAll( Arrays.asList(variant1,variant2,variant3));
		
		
		
		ColorVo color = new ColorVo(30000l, "Nexa Blue");
		ColorVo color1 = new ColorVo(30001l, "Pearl Metalic");
		ColorVo color2 = new ColorVo(30002l, "Midnight Black");
		ColorVo color3 = new ColorVo(30003l, "Magma Gray");
		ColorVo color4 = new ColorVo(30004l, "Premium Silver");
		ColorVo color5 = new ColorVo(30005l, "Sangaria Red");
		
		colorRepo.saveAll(Arrays.asList(color,color1,color2,color3,color4,color5));
		
		VariantColorVo variantColor = new VariantColorVo(3, variant1, color);
		VariantColorVo variantColor1 = new VariantColorVo(2, variant1, color1);
		VariantColorVo variantColor2 = new VariantColorVo(5, variant1, color2);
		VariantColorVo variantColor3 = new VariantColorVo(5, variant1, color3);
		VariantColorVo variantColor4 = new VariantColorVo(6, variant1, color4);
		VariantColorVo variantColor5 = new VariantColorVo(2, variant1, color5);
		
		VariantColorVo variantColor6 = new VariantColorVo(2, variant2, color);
		VariantColorVo variantColor7 = new VariantColorVo(5, variant2, color1);
		VariantColorVo variantColor8 = new VariantColorVo(2, variant2, color2);
		VariantColorVo variantColor9 = new VariantColorVo(1, variant2, color3);
		VariantColorVo variantColor0 = new VariantColorVo(2, variant2, color4);
		VariantColorVo variantColor11 = new VariantColorVo(2, variant2, color5);
		
		VariantColorVo variantColor12 = new VariantColorVo(2, variant3, color);
		VariantColorVo variantColor13 = new VariantColorVo(2, variant3, color1);
		VariantColorVo variantColor14 = new VariantColorVo(3, variant3, color2);
		VariantColorVo variantColor15= new VariantColorVo(3, variant3, color3);
		VariantColorVo variantColor16 = new VariantColorVo(1, variant3, color4);
		VariantColorVo variantColor17= new VariantColorVo(2, variant3, color5);
		
		
		variantColorRepo.saveAll(Arrays.asList(variantColor,variantColor1,variantColor2,variantColor3,variantColor4,variantColor5,
				variantColor6,variantColor7,variantColor8,variantColor9,variantColor0,variantColor11,variantColor12,variantColor13,
				variantColor14,variantColor15,variantColor16,variantColor17));
		
		
		log.info("Inserted Mock data in DB");
		
		
	}


	@Override
	public void run(String... args) throws Exception {
		
		insertVehicle();
		
		
	}

}
