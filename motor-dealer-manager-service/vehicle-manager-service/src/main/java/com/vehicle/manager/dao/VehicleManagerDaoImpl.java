package com.vehicle.manager.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vehicle.manager.model.ColorVo;
import com.vehicle.manager.model.VariantColorId;
import com.vehicle.manager.model.VariantColorVo;
import com.vehicle.manager.model.VariantVo;
import com.vehicle.manager.model.VehicleVo;
import com.vehicle.manager.repo.ColorRepo;
import com.vehicle.manager.repo.VariantColorRepo;
import com.vehicle.manager.repo.VariantRepo;
import com.vehicle.manager.repo.VehicleRepo;
import com.vehicle.manager.util.VehicleVtoMapper;
import com.vehicle.manager.vto.VehicleVto;

@Component
public class VehicleManagerDaoImpl implements VehicleManagerDao {

	@Autowired
	VehicleRepo vehicleRepo;

	@Autowired
	VariantColorRepo variantColorRepo;
	
	@Autowired
	VariantRepo variantRepo; 
	
	@Autowired
	ColorRepo colorRepo; 

	Logger log = LoggerFactory.getLogger(getClass());
	
	
	public List<VehicleVo> getFullVehicleList() {
		log.info("Getting Full vehicle list from Dao");
		List<VehicleVo> vehicleVoList = vehicleRepo.findAll();
		log.info("Getting Full vehicle list from Dao completed");
		return vehicleVoList;
	}

	public VehicleVo insertVehicle(VehicleVo vehicleVo) {
		log.info("Inserting  vehicle in Dao : VehicleId {}",vehicleVo.getVehicleId() );
		VehicleVo vehicleVoInserted = vehicleRepo.save(vehicleVo);
		log.info("Inserting Vehicle in Dao is success");
		return vehicleVoInserted;

	}
	
	public VariantVo insertUpdateVariant(VariantVo variantVo)
	{
		log.info("Inserting/Updating Variant in Dao : variantId {}",variantVo.getVariantId() );
		return variantRepo.save(variantVo);
	}
	
	public ColorVo insertUpdateColor(ColorVo colorVo)
	{
		log.info("Inserting / Updating ColorVo in Dao {}", colorVo.getColorCode());
		return colorRepo.save(colorVo);
	}
	
	public VariantColorVo insertUpdateVariantColor(VariantColorVo variantColorVo)
	{
		log.info("Inserting into VariantColorVo {} - {} - {}",variantColorVo.getVariantId(), variantColorVo.getColorCode(), variantColorVo.getAvailableStock());
		return variantColorRepo.save(variantColorVo);
	}
	
	public void deleteVehicle(Long vehicleId)
	{
		log.info("Deleting the Vehicle :{}", vehicleId);
		List<Long> variantsList = variantRepo.findAllIdByVechicleId(vehicleId);
		
		for(Long variantId : variantsList)
		{
			variantColorRepo.deleteByVariantId(variantId);
		}
		
		variantRepo.deleteVariantByVehicleId(vehicleId);
		vehicleRepo.deleteById(vehicleId);
	}
	
	public void deleteVariant(Long variantId)
	{
		log.info("Deleting the variant {}", variantId);
		variantColorRepo.deleteByVariantId(variantId);
		variantRepo.deleteById(variantId);
	}
	
	public void deleteColor(Long colorCode)
	{
		log.info("Deleting the color {}", colorCode);
		colorRepo.deleteById(colorCode);
	}
	
	public void deleteVariantColor(Long variantId, Integer colorCode)
	{
		log.info("Deleting variant color record by variantId- {} and ColorCode- {}", variantId, colorCode);
		variantColorRepo.deleteByVariantAndColor(variantId, colorCode);
	}
	

}
