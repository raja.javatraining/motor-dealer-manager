package com.vehicle.manager.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data @AllArgsConstructor
@NoArgsConstructor
@Table(name ="colors")
public class ColorVo {
	
	@Id
	@Column(name="color_code")
	private Long colorCode;
	private String colorName;
	
	
	@OneToMany (fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.REFRESH,CascadeType.REMOVE}, mappedBy = "color")
	private List<VariantColorVo> colorsList;

	public ColorVo(Long colorCode, String colorName) {
		super();
		this.colorCode = colorCode;
		this.colorName = colorName;
	} 
	
	
	
	

}
