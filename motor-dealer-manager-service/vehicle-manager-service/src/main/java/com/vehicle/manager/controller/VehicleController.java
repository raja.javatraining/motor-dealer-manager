package com.vehicle.manager.controller;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vehicle.manager.dao.VehicleManagerDao;
import com.vehicle.manager.model.VariantColorVo;
import com.vehicle.manager.service.VehicleService;
import com.vehicle.manager.vto.VehicleVto;

@CrossOrigin(origins="*")
@RestController
public class VehicleController {
	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	VehicleService vehicleService; 
	
	@GetMapping(value="/")
	public String health()
	{
		return "OK"; 
	}
	
	
	@GetMapping(value = "/allVehicles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<VehicleVto> getAllVehicle()
	{
		log.info("calling getAllVehicle() method from controller");
		return vehicleService.getFullVehicleList(); 
	}
	
	@GetMapping(value = "/vehicle/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VehicleVto getVehicleById(@PathVariable("id") Long id)
	{
		log.info("calling getVehicleById method from controller");
		return vehicleService.getVehicleById(id); 
	}
	
	@DeleteMapping(value="/vehicle/{id}")
	public ResponseEntity<String> deleteVehicle(@PathVariable("id") Long id)
	{
		log.info("Delete Vehicle requested for VehicleId {}",id);
		vehicleService.deleteVehicle(id);
		return new ResponseEntity<>(HttpStatus.OK).ok("Vehicle Deleted") ; 
	}
	
	@DeleteMapping(value="/variant/{id}")
	public ResponseEntity<String> deleteVariant(@PathVariable("id") Long id)
	{
		log.info("Delete Variant requested for VehicleId {}",id);
		vehicleService.deleteVariant(id);
		return new ResponseEntity<>(HttpStatus.OK).ok("Variant Deleted") ; 
	}
	
	@DeleteMapping(value="/color/{id}")
	public ResponseEntity<String> deleteColor(@PathVariable("id") Long id)
	{
		log.info("Delete Color requested for VehicleId {}",id);
		vehicleService.deleteColor(id);
		return new ResponseEntity<>(HttpStatus.OK).ok("Color Deleted") ; 
	}
	
	
	@PostMapping(value="/vehicle", consumes= MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> insertVehicle(@RequestBody VehicleVto vehicleVto)
	{
		log.info("Inserting Vehicle requeted"); 
		
		
		String status = vehicleService.insertVehicle(vehicleVto)?"Error":"Inserted" ;
		
		Map<String, Object> body = new LinkedHashMap<>();
	    body.put("timestamp", LocalDateTime.now());
	    body.put("message", status);
	    
	    log.info("Inserting Status: "+status); 
	
	    if("Inserted".equals(status))
	    {
	    return new ResponseEntity<>(body, HttpStatus.ACCEPTED);
	    }
	    else
	    {
	    	 return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
		
		
	}
	
	@PutMapping(value="/vehicle", consumes= MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> updateVehicle(@RequestBody VehicleVto vehicleVto)
	{
		log.info("Updating vehicle requested");
		ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.ACCEPTED); 
		response.ok("Updated");
		
		return vehicleService.insertVehicle(vehicleVto) ? response
				:new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	

}
