package com.vehicle.manager;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.vehicle.manager.controller.VehicleController;
import com.vehicle.manager.model.ColorVo;
import com.vehicle.manager.model.VariantColorVo;
import com.vehicle.manager.model.VariantVo;
import com.vehicle.manager.model.VehicleVo;
import com.vehicle.manager.repo.ColorRepo;
import com.vehicle.manager.repo.VariantColorRepo;
import com.vehicle.manager.repo.VariantRepo;
import com.vehicle.manager.repo.VehicleRepo;
import com.vehicle.manager.util.VehicleVtoMapper;
import com.vehicle.manager.vto.VehicleVto;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
class VehicleManagerServiceApplicationTests {
	
	

	@Autowired
	VehicleController vehicleController; 
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	  private WebApplicationContext webApplicationContext;
	
	
	// Check application is loading properly
	@Test
	@Order(1)
	void contextLoads() {
		assertThat(vehicleController).isNotNull();
	}
	
	@Test
	@Order(2)
	public void pathTest() throws Exception {
		this.mvc.perform(get("/")).andExpect(status().isOk())
				.andExpect(content().string("OK"));
	}

	@Test
	@Order(3)
	public void getAllVehicleAPI() throws Exception 
	{
	  mvc.perform( MockMvcRequestBuilders
	      .get("/allVehicles")
	      .accept(MediaType.APPLICATION_JSON))
	      .andDo(print())
	      .andExpect(status().isOk())
	      .andExpect(MockMvcResultMatchers.jsonPath("$[*].vehicleId").isNotEmpty());
	}

	
	
	
	@Test
	@Order(4)
	public void deleteColorSuccess() throws Exception
	{
		mvc.perform( MockMvcRequestBuilders
			      .delete("/color/"+30000))
			      .andExpect(status().isOk())
			      .andExpect(content().string("Color Deleted"));
	}
	
	
	@Test
	@Order(5)
	public void deleteColorFailed() throws Exception
	{
		mvc.perform( MockMvcRequestBuilders
					.delete("/color/"+30000))
			      .andExpect(status().isBadRequest());
	}
	
	@Test
	@Order(6)
	public void deleteVariantSuccess() throws Exception
	{
		mvc.perform( MockMvcRequestBuilders
			      .delete("/variant/"+2020010101))
			      .andExpect(status().isOk())
			      .andExpect(content().string("Variant Deleted"));
	}
	
	
	@Test
	@Order(7)
	public void deleteVariantFailed() throws Exception
	{
		mvc.perform( MockMvcRequestBuilders
				 .delete("/variant/"+2020010101))
			      .andExpect(status().isBadRequest());
	}
	
	@Test
	@Order(8)
	public void deleteVehicleSuccess() throws Exception
	{
		mvc.perform( MockMvcRequestBuilders
			      .delete("/vehicle/"+20200101))
			      .andExpect(status().isOk())
			      .andExpect(content().string("Vehicle Deleted"));
	}
	
	
	@Test
	@Order(9)
	public void deleteVehicleFailed() throws Exception
	{
		mvc.perform( MockMvcRequestBuilders
			      .delete("/vehicle/"+20200))
			      .andExpect(status().isBadRequest());
	}
	
	
	@Test
	@Order(10)
	public void insertVehicle() throws Exception 
	{
	
		String jsonInp = "{"
				+ "    \"vehicleId\": 20200101,"
				+ "    \"vehicleName\": \"Suzuki\","
				+ "    \"model\": \"Ciaz\","
				+ "    \"makeYear\": 2021,"
				+ "    \"vehicleTotalAvailableStock\": 50,"
				+ "    \"variants\": ["
				+ "        {"
				+ "            \"variantId\": 2020010101,"
				+ "            \"variantName\": \"VXI\","
				+ "            \"transmission\": \"7-Speed DCT\","
				+ "            \"fuelType\": \"petrol\","
				+ "            \"engineModel\": \"1.0l Turbo\","
				+ "            \"variantTotalAvailableStock\": 23,"
				+ "            \"colors\": ["
				+ "                {"
				+ "                    \"colorCode\": 30000,"
				+ "                    \"colorName\": \"Nexa Blue\","
				+ "                    \"stock\": 3"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30001,"
				+ "                    \"colorName\": \"Pearl Metalic\","
				+ "                    \"stock\": 2"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30002,"
				+ "                    \"colorName\": \"Midnight Black\","
				+ "                    \"stock\": 5"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30003,"
				+ "                    \"colorName\": \"Magma Gray\","
				+ "                    \"stock\": 5"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30004,"
				+ "                    \"colorName\": \"Premium Silver\","
				+ "                    \"stock\": 6"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30005,"
				+ "                    \"colorName\": \"Sangaria Red\","
				+ "                    \"stock\": 2"
				+ "                }"
				+ "            ]"
				+ "        },"
				+ "        {"
				+ "            \"variantId\": 2020010102,"
				+ "            \"variantName\": \"LXI\","
				+ "            \"transmission\": \"7-Speed DCT\","
				+ "            \"fuelType\": \"petrol\","
				+ "            \"engineModel\": \"1.0l Turbo\","
				+ "            \"variantTotalAvailableStock\": 14,"
				+ "            \"colors\": ["
				+ "                {"
				+ "                    \"colorCode\": 30000,"
				+ "                    \"colorName\": \"Nexa Blue\","
				+ "                    \"stock\": 2"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30001,"
				+ "                    \"colorName\": \"Pearl Metalic\","
				+ "                    \"stock\": 5"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30002,"
				+ "                    \"colorName\": \"Midnight Black\","
				+ "                    \"stock\": 2"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30003,"
				+ "                    \"colorName\": \"Magma Gray\","
				+ "                    \"stock\": 1"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30004,"
				+ "                    \"colorName\": \"Premium Silver\","
				+ "                    \"stock\": 2"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30005,"
				+ "                    \"colorName\": \"Sangaria Red\","
				+ "                    \"stock\": 2"
				+ "                }"
				+ "            ]"
				+ "        },"
				+ "        {"
				+ "            \"variantId\": 2020010103,"
				+ "            \"variantName\": \"ZXI\","
				+ "            \"transmission\": \"7-Speed DCT\","
				+ "            \"fuelType\": \"petrol\","
				+ "            \"engineModel\": \"1.0l Turbo\","
				+ "            \"variantTotalAvailableStock\": 13,"
				+ "            \"colors\": ["
				+ "                {"
				+ "                    \"colorCode\": 30000,"
				+ "                    \"colorName\": \"Nexa Blue\","
				+ "                    \"stock\": 2"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30001,"
				+ "                    \"colorName\": \"Pearl Metalic\","
				+ "                    \"stock\": 2"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30002,"
				+ "                    \"colorName\": \"Midnight Black\","
				+ "                    \"stock\": 3"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30003,"
				+ "                    \"colorName\": \"Magma Gray\","
				+ "                    \"stock\": 3"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30004,"
				+ "                    \"colorName\": \"Premium Silver\","
				+ "                    \"stock\": 1"
				+ "                },"
				+ "                {"
				+ "                    \"colorCode\": 30005,"
				+ "                    \"colorName\": \"Sangaria Red\","
				+ "                    \"stock\": 2"
				+ "                }"
				+ "            ]"
				+ "        }"
				+ "    ]"
				+ "}";
	
		
	  mvc.perform( MockMvcRequestBuilders
	      .post("/vehicle")
	      .content(jsonInp)
	      .contentType(MediaType.APPLICATION_JSON)
	      .accept(MediaType.APPLICATION_JSON))
	      .andExpect(status().isAccepted())
	      .andExpect(MockMvcResultMatchers.jsonPath("$.message").exists());
	}
	 

	
	// Testing insert succcess scenario
	@Test
	void testInsertFailed() throws Exception {
		
		String jsonInput="{"
				+ "    'vehicleId': 123,"
				+ "    'vehicleName': 'Hyundai',"
				+ "    'model': 'VERNA',"
				+ "    'makeYear': 2013"
				+ "}";
		
		ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
	    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
	    String requestJson=ow.writeValueAsString(jsonInput );
	    
	    System.out.println(requestJson);
	    
	    mvc.perform(post("/vehicle")
		           .contentType(MediaType.APPLICATION_JSON)
		           .content(requestJson) 
		           .accept(MediaType.APPLICATION_JSON))
		           .andExpect(status().isBadRequest());
	}

}
