# motor-dealer-manager-service
This application is Spring boot Rest service 

# --------------------------------------
Using In Memory Database
# --------------------------------------
# Enabling H2 Console in Application.yml
spring:
      h2:
        console:
          enabled:  true
      datasource:
        url: jdbc:h2:mem:testdb
      data:
        jpa:
          repositories:
            bootstrap-mode: default

---- pom.xml. ------
			
			<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>

---- Configuration -------
http://localhost:8080/h2-console/



Give JDBC:URL --> jdbc:h2:mem:testdb



