create database motor_dealer;
use motor_dealer;

drop table vehicle;
drop table colors;
drop table variant;
drop table variant_colors;

delete  from variant;

ALTER TABLE colors
  DROP foreign key variant_id ;
  
  desc colors;

create table vehicle( vehicle_id bigint primary key not null, vehicle_name varchar(30), model varchar(30),make_year int);
create table variant( variant_id bigint primary key not null, variant_name varchar(30), fuel_type varchar(10), transmission varchar(20), 
engine_model varchar(30),vehicle_id bigint, foreign key (vehicle_id) references vehicle(vehicle_id));
create table colors(color_code bigint primary key not null, color_name varchar(30));
create table variant_colors (variant_id bigint , color_code bigint, available_stock int, 
 foreign key (variant_id) references variant(variant_id), foreign key (color_code) references colors(color_code));

insert into vehicle values (20200101, "Hyundai", "VERNA", 2020);
insert into variant values (2020010101,'SX(O) Turbo DCT','petro','7-Speed DCT', '1.0l Kappa Turbo GDi', 20200101 );
insert into colors values (1000,'Feiry Red' );
insert into variant_colors values (2020010101,1000,10);
select * from variant where variant_id = 2018010101;

commit;

select vr.variant_name,cr.color_name,vc.available_stock from variant_colors vc 
join variant vr on vr.variant_id=vc.variant_id 
join colors cr on cr.color_code = vc.color_code
order by vr.variant_id;

SELECT variant_id,SUM(available_stock) 
FROM variant_colors 
GROUP BY variant_id;


select * from vehicle;
select * from variant; 
select * from colors; -- Many to Many 
select * from variant_colors;

select * from variant where vehicle_id=20130101; -- Venue







 