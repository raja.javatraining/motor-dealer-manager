import { Component, OnInit } from '@angular/core';
import vehicle from './../_files/vehicles.json';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent implements OnInit {
  constructor() {}
  public modalBodyContent = vehicle;
  ngOnInit(): void {}
  displayStyle = 'none';

  openPopup(item: any) {
    this.modalBodyContent = item;
    console.log(this.modalBodyContent);
    this.displayStyle = 'block';
  }
  closePopup() {
    this.displayStyle = 'none';
  }
}
