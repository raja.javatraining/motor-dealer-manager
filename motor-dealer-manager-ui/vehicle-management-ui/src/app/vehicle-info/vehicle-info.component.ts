import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import countries from './../_files/countries.json';
import vehicle from './../_files/vehicles.json';
import * as $ from 'jquery';

@Component({
  selector: 'app-vehicle-info',
  templateUrl: './vehicle-info.component.html',
  styleUrls: ['./vehicle-info.component.css'],
})
export class VehicleInfoComponent implements OnInit {
  public countryList: { name: string; code: string }[] = countries;
  public vehicleList: {
    brand: String;
    model: String;
    year: String;
    variant: String;
    engine: String;
    fuel: String;
    transmission: String;
  }[] = vehicle;

  public modalBodyContent = vehicle;
  constructor() {}
  displayStyle = 'none';

  openPopup(item: any) {
    this.modalBodyContent = item;
    console.log(this.modalBodyContent);
    this.displayStyle = 'block';
  }
  closePopup() {
    this.displayStyle = 'none';
  }

  ngOnInit(): void {}
}
