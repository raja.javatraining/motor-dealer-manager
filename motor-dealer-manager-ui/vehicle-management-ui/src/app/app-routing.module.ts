import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleInfoComponent } from './vehicle-info/vehicle-info.component';

const routes: Routes = [{path: 'vehicle', component: VehicleInfoComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
