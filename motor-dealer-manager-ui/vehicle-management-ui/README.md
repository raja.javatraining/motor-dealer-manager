# VehicleManagementUi
This app is generated in Angular. A Node based application. 
Before downloading make sure the proper node / npm veresions are installed. 

For doing CRUD operations for Vehicle management Service running in spring Boot. 

## Follow steps to use this code in local 
1. Download the code and place it in local directory
2. Open that directory in VSCode. 
3. In VSCode terminal locate the directory or make sure the directory which is having package.json
4. type "ng serve --open" on terminal to run the application 
    a) Make sure @angular/cli installed
    b) use "npm install jquery" to install jquery as a component