import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehicle-table',
  templateUrl: './vehicle-table.component.html',
  styleUrls: ['./vehicle-table.component.css'],
})
export class VehicleTableComponent implements OnInit {
  public modalBodyContent: any = {};
  public action={
    buttonName: "",
    actionEndpoint: ""
  };
  public modalElements={
      title: ""
  }
  constructor() {}

  loadEditModal(item: any) {
    this.modalBodyContent = item;
    this.modalElements={ title: "Update"}
    this.action={
        buttonName: "Update",
        actionEndpoint: "/updateurl"
    };
    console.log(this.modalBodyContent);
  }

  loadNewModal() {
    this.modalBodyContent = {};
    this.modalElements={ title: "Insert New Vehicle"}
    this.action={
        buttonName: "Insert",
        actionEndpoint: "/inserturl"
    };
    
  }
  loadModal(item: any) {
    this.modalBodyContent = item;
    
  }
  loadDeleteModal(item: any){
    this.modalBodyContent = item;
    this.modalElements={ title: "Delete"}
    this.action={
        buttonName: "Insert",
        actionEndpoint: "/inserturl"
    };
  }

  ngOnInit(): void {}

  vehicleInfo: any = [
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  SX(O) Turbo DCT',
      engine: '1.0l Kappa Turbo GDi',
      fuel: ' Petrol',
      transmission: '7-Speed DCT',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  E',
      engine: '1.5l MPi',
      fuel: ' Petrol',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  S+',
      engine: '1.5l MPi',
      fuel: ' Petrol',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  SX',
      engine: '1.5l MPi',
      fuel: ' Petrol',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  SX(O)',
      engine: '1.5l MPi',
      fuel: ' Petrol',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  SX IVT',
      engine: '1.5l MPi',
      fuel: ' Petrol',
      transmission: ' IVT',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  SX(O) IVT',
      engine: '1.5l MPi',
      fuel: ' Petrol',
      transmission: ' IVT',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  DSL SX AT',
      engine: '1.5lU2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Automatic',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  DSL SX(O) AT',
      engine: '1.5lU2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Automatic',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  DSL S+',
      engine: '1.5lU2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  DSL SX',
      engine: '1.5lU2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VERNA',
      year: '2020',
      variant: '  DSL SX(O)',
      engine: '1.5lU2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.0T Asta(O) DCT DT',
      engine: '1.0 Turbo GDi',
      fuel: ' Petrol',
      transmission: ' DCT',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.0T Asta DCT DT',
      engine: '1.0 Turbo GDi',
      fuel: ' Petrol',
      transmission: ' DCT',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.0T Asta iMT DT',
      engine: '1.0 Turbo GDi',
      fuel: ' Petrol',
      transmission: ' IMT',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.0T Sports iMT DT',
      engine: '1.0 Turbo GDi',
      fuel: ' Petrol',
      transmission: ' IMT',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.2 Asta DT',
      engine: '1.2 l Kappa',
      fuel: ' Petrol',
      transmission: '5-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.2 Asta(O) DT',
      engine: '1.2 l Kappa',
      fuel: ' Petrol',
      transmission: '5-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.2 Sports DT',
      engine: '1.2 l Kappa',
      fuel: ' Petrol',
      transmission: '5-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.2 Asta iVT DT',
      engine: '1.2 l Kappa',
      fuel: ' Petrol',
      transmission: ' IVT',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.2 Sports iVT DT',
      engine: '1.2 l Kappa',
      fuel: ' Petrol',
      transmission: ' IVT',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.5 DSL Asta(O) DT',
      engine: '1.5 l U2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'I20',
      year: '2020',
      variant: '1.5 DSL Sportz DT',
      engine: '1.5 l U2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  S(O)',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: '7-Speed DCT',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX+',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: '7-Speed DCT',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX+ Dual Tone',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: '7-Speed DCT',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  S(O)',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: 'IMT',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: 'IMT',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX Dual Tone',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: 'IMT',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX(O)',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: 'IMT',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX(O) Dual Tone',
      engine: '1.0 l Kappa Turbo GDi',
      fuel: ' Petrol ',
      transmission: 'IMT',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  E',
      engine: '1.2 l Kappa',
      fuel: ' Petrol',
      transmission: '5-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  S',
      engine: '1.2 l Kappa',
      fuel: ' Petrol',
      transmission: '5-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  S',
      engine: '1.2 l Kappa',
      fuel: ' Petrol',
      transmission: '5-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  S(O) DSL',
      engine: '1.5 l U2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX DSL',
      engine: '1.5 l U2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX Dual Tone DSL',
      engine: '1.5 l U2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX(O) DSL',
      engine: '1.5 l U2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX(O) Dual Tone DSL',
      engine: '1.5 l U2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
    {
      brand: 'Hyundai',
      model: 'VENUE',
      year: '2021',
      variant: '  SX(O) Executive',
      engine: '1.5 l U2 CRDi',
      fuel: ' Diesel',
      transmission: '6-Speed Manual',
    },
  ];
}
