import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VehicleEditFormComponent } from '../vehicle-edit-form/vehicle-edit-form.component';

@Component({
  selector: 'app-vehicle-info',
  templateUrl: './vehicle-info.component.html',
  styleUrls: ['./vehicle-info.component.css'],
})
export class VehicleInfoComponent implements OnInit {
  public modalBodyContent: any = {};
  public modalHeader: string = '';
  public vehicleInfo: any = [];
  public action = {
    buttonName: '',
    actionEndpoint: '',
  };
  public modalElements = {
    title: '',
  };
  public editComponent = false; 
    _router: any;
    displayTable: boolean = false;
    
 
    loadEditModal(item: any) {
    this.modalBodyContent = item;
    this.editComponent=true; 
    this.modalElements = { title: 'Update' };
    this.action = {
      buttonName: 'Update',
      actionEndpoint: '/updateurl',
    };
   
    console.log(this.modalBodyContent);
  }

  loadNewModal() {
    this.modalBodyContent = {};
    this.modalElements = { title: 'Insert New Vehicle' };
    this.action = {
      buttonName: 'Insert',
      actionEndpoint: '/inserturl',
    };
  }
  loadModal(model: '', variant: any) {
    this.modalHeader = model;
    this.modalBodyContent = variant;
  }
  loadDeleteModal(item: any) {
      console.log(item);
    this.modalBodyContent = item;
    this.modalElements = { title: 'Delete' };
    this.action = {
      buttonName: 'Insert',
      actionEndpoint: '/inserturl',
    };
  }

  constructor(private http: HttpClient) {
    this.getAllVehiclesFromApi();
  }

  getAllVehiclesFromApi() {
    var url = 'http://localhost:7001/motordealer/allVehicles';
    //var url ='http://localhost:4200/assets/mock-data.json';

    this.http
      .get<any>(url)
      .subscribe((data) => {
        this.vehicleInfo = data;
        this.displayTable = true;
      })
      ;
  }

  deleteVehicle(id:string|number){
      this.http
      .delete<any>('http://localhost:7001/motordealer/vehicle/'+id)
      .subscribe((data) => {
          console.log(data);
         
      });
      alert("Record Deleted");
      this.getAllVehiclesFromApi();
  }

  reloadCurrentRoute() {
    let currentUrl = this._router.url;
    this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this._router.navigate([currentUrl]);
        console.log(currentUrl);
    });
  }
  
  ngOnInit(): void {}

}
