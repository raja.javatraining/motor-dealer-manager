import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-sample-form',
  templateUrl: './sample-form.component.html',
  styleUrls: ['./sample-form.component.css']
})
export class SampleFormComponent implements OnInit {

  survey: FormGroup |any;
  constructor() {

  }

  ngOnInit() {
    this.survey = new FormGroup({
      surveyName: new FormControl(''),
      logoUrl: new FormControl(''),
      headerUrl: new FormControl(''),
      headerColor: new FormControl(''),
      footerUrl: new FormControl(''),
      footerColor: new FormControl(''),
      sections: new FormArray([
        this.initSection(),
      ]),
    });
  }

  initSection() {
    return new FormGroup({
      sectionTitle: new FormControl(''),
      sectionDescription: new FormControl(''),
      questions: new FormArray([
        this.initQuestion()
        ])
    });
  }
  initQuestion() {
    return new FormGroup({
      questionTitle: new FormControl(''),
      questionType: new FormControl('',Validators.required),
      options: new FormArray([
        this.initOptions()
      ])
    });
  }

  initOptions() {
    return new FormGroup({
      optionTitle: new FormControl('')
    });
  }

  addSection() {
    const control = <FormArray>this.survey.get('sections');
    control.push(this.initSection());
  }

  addQuestion(j: string | number) {
    console.log(j);
    const control = <FormArray>this.survey.get('sections').controls[j].get('questions');
   // console.log(control);
    control.push(this.initQuestion());
    
  }

  add(i: string | number,j: string | number) {
    //console.log(k);
    const control = <FormArray>this.survey.get('sections').controls[i].get('questions').controls[j].get('options');

  // const control = <FormArray>this.survey.get(['sections',0,'questions',k,'options']); // also try this new syntax
    //console.log(control);
    control.push(this.initOptions());
  }

  getSections(form: { controls: { sections: { controls: any; }; }; }) {
    //console.log(form.get('sections').controls);
    return form.controls.sections.controls;
  }
  getQuestions(form: { controls: { questions: { controls: any; }; }; }) {
   //console.log(form.controls.questions.controls);
    return form.controls.questions.controls;
  }
  getOptions(form: { controls: { options: { controls: any; }; }; }) {
    //console.log(form.get('options').controls);
    return form.controls.options.controls;

  }

  removeQuestion(j: number){
     const control = <FormArray>this.survey.get('sections').controls[j].get('questions');
     control.removeAt(j);
  }

  removeSection(i: number){
   const control = <FormArray>this.survey.get('sections');
   control.removeAt(i);

  }

  removeOption(i: any,j: any,k: number){
    console.log(i,j,k);
   const control = <FormArray>this.survey.get(['sections',i,'questions',j,'options']); // also try this new syntax
   control.removeAt(k);
  }

  remove(i: any,j: any){
    const control =  <FormArray>this.survey.get(['sections',i,'questions',j,'options']);
    control.removeAt(0);
    control.controls = [];
  }

  onSubmit(form: any){
    
  }

}
