export class Vehicle {
    vehicleId!: number;
    vehicleName!: string;
    variant!: Variant[];
}

export class Variant{
    variantId!: number; 
    variantName!: string; 
    fuelType!: string; 
    colors!: Color[];
}

export class Color{
    colorCode!: number;
    colorName!: string;
    stock!:number;
}