import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VehicleTableComponent } from './vehicle-table/vehicle-table.component';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { EditModalFormComponent } from './edit-modal-form/edit-modal-form.component';
import { VehicleInfoComponent } from './vehicle-info/vehicle-info.component';
import { ReactiveFormsModule } from '@angular/forms';
import { VehicleFormComponent } from './vehicle-form/vehicle-form.component';
import { SampleFormComponent } from './sample-form/sample-form.component';
import { VehicleEditFormComponent } from './vehicle-edit-form/vehicle-edit-form.component';


@NgModule({
  declarations: [
    AppComponent,
    VehicleTableComponent,
    EditModalFormComponent,
    VehicleInfoComponent,
    VehicleFormComponent,
    SampleFormComponent,
    VehicleEditFormComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DataTablesModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
