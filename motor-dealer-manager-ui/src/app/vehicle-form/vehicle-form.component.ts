import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  FormArray,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { VehicleInfoComponent } from '../vehicle-info/vehicle-info.component';



@Component({
  selector: 'app-vehicle-form',
  templateUrl: './vehicle-form.component.html',
  styleUrls: ['./vehicle-form.component.css'],
})
export class VehicleFormComponent implements OnInit {
  
  vehicle: FormGroup | any;

  vehicleComponent :  VehicleInfoComponent|any;
  constructor(private http: HttpClient) {}


  
  ngOnInit(): void {
    this.vehicle = new FormGroup({
      vehicleId: new FormControl(''),
      vehicleName:new FormControl(''),
      model: new FormControl(''),
      makeYear: new FormControl(''),
      variants: new FormArray([
        this.initVariant()
      ])
      
    });
  }

  initVariant(){
    return new FormGroup({
      variantId: new FormControl(), 
      variantName: new FormControl(),
      fuelType:new FormControl(),
      engineModel:new FormControl(),
      transmission: new FormControl(),
      colors: new FormArray([
        this.initColor()
      ])
    });
  }

  initColor(){
    return new FormGroup({
      colorCode: new FormControl(),
      colorName: new FormControl(),
      stock: new FormControl()
    });
  }

  addVariant(){
    const control = <FormArray>this.vehicle.get('variants');
    control.push(this.initVariant());
  }

  addColor(i: string|number){
    const control = <FormArray>this.vehicle.get('variants').controls[i].get('colors');
    // console.log(control);
     control.push(this.initColor());
  }  

  removeVariant(i: number){
    const control = <FormArray>this.vehicle.get('variants');
    control.removeAt(i);
  }

  removeColor(i:number, j: number) {

    console.log(this.vehicle.get('variants').controls[i]);
    const control = <FormArray>(
      this.vehicle.get('variants').controls[i].get('colors')
    );
    // console.log(control);
    control.removeAt(j);
  }

  getVariants(form: { controls: { variants: { controls: any; }; }; })
  {
    return form.controls.variants.controls;
  }

  getColors(form: {controls:{colors:{controls:any;};};})
  {
    return form.controls.colors.controls;
  }
  
  addNewVehicleSubmit()
  {
    console.log(this.vehicle.value);
    this.http
    .post<any>('http://localhost:7001/motordealer/vehicle',this.vehicle.value)
    .subscribe((data) => {
     console.log(data); 
     alert("Vehicle Added");
     window.location.reload();
    }) ;
  }

}

