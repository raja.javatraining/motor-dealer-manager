import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditModalFormComponent } from './edit-modal-form.component';

describe('EditModalFormComponent', () => {
  let component: EditModalFormComponent;
  let fixture: ComponentFixture<EditModalFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditModalFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditModalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
