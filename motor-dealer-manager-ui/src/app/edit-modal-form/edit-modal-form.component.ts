import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-modal-form',
  templateUrl: './edit-modal-form.component.html',
  styleUrls: ['./edit-modal-form.component.css']
})
export class EditModalFormComponent {
 @Input() vehicleInfo = {
   brand: undefined,
   model: undefined,
   year: undefined,
   variant: undefined,
   engine: undefined,
   fuel: undefined,
   transmission: undefined
 };
 @Input() action: any = {}
 
 constructor() { }
 ngOnInit() {}
}
