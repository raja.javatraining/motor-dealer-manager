import {
  AfterContentChecked,
  AfterContentInit,
  Component,
  ContentChild,
  Input,
  OnInit,
} from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { VehicleInfoComponent } from '../vehicle-info/vehicle-info.component';

@Component({
  selector: 'app-vehicle-edit-form',
  templateUrl: './vehicle-edit-form.component.html',
  styleUrls: ['./vehicle-edit-form.component.css'],
})
export class VehicleEditFormComponent implements OnInit {
  vehicle: FormGroup | any;

  constructor(private http: HttpClient) {}

  @Input() vehicleInfo: any = {};

  ngOnChanges() {
    if (this.vehicleInfo) {
      console.log(this.vehicleInfo);
      this.initVehicle(this.vehicleInfo);
    }
  }

  ngOnInit(): void {
    console.log(this.vehicleInfo);
    this.initVehicle(this.vehicleInfo);
  }

  initVehicle(vehicle: any) {
    this.vehicle = new FormGroup({
      vehicleId: new FormControl(vehicle.vehicleId),
      vehicleName: new FormControl(vehicle.vehicleName),
      model: new FormControl(vehicle.model),
      makeYear: new FormControl(vehicle.makeYear),
      variants: new FormArray(
        vehicle.variants.map((variant: any) => this.initVariant(variant))
      ),
    });
  }

  initVariant(variant: any = {}) {
    return new FormGroup({
      variantId: new FormControl(variant.variantId),
      variantName: new FormControl(variant.variantName),
      fuelType: new FormControl(variant.fuelType),
      engineModel: new FormControl(variant.engineModel),
      transmission: new FormControl(variant.transmission),
      colors: new FormArray(
        variant.colors.map((color: any) => this.initColor(color))
      ),
    });
  }

  initColor(color: any = {}) {
    return new FormGroup({
      colorCode: new FormControl(color.colorCode),
      colorName: new FormControl(color.colorName),
      stock: new FormControl(color.stock),
    });
  }

  addVariant() {
    const control = <FormArray>this.vehicle.get('variants');
    control.push(this.initVariant());
  }

  addColor(i: string | number) {
    const control = <FormArray>(
      this.vehicle.get('variants').controls[i].get('colors')
    );
    // console.log(control);
    control.push(this.initColor());
  }

  removeVariant(i: number) {
    var inp = confirm("Are you sure to delete this Variant. ('Cannot undo the delete')")
    if(inp)
    {
      const control = <FormArray>this.vehicle.get('variants');
      let id= control.value[i].variantId;
      this.http
      .delete<any>('http://localhost:7001/motordealer/variant/'+id)
      .subscribe((error) => {
          alert("Service Response :"+error);
          console.log(error);
      } );
      control.removeAt(i);
    }
    
  }

  removeColor(i: number, j: number) {
    console.log(this.vehicle.get('variants').controls[i].get('colors').value[j].colorCode);
    var inp = confirm("Are you sure to delete this Color. ('Cannot undo the delete')")
    if(inp)
    {
    const control = <FormArray>(this.vehicle.get('variants').controls[i].get('colors'));
    let id= control.value[j].colorCode;
    this.http
      .delete<any>('http://localhost:7001/motordealer/color/'+id)
      .subscribe({
        next: data => {
          alert("Service Response :"+data);
          console.log(data);
      },
      error: error => {
        alert("Service Response :"+error);
        console.log(error);
      }
          
      })
      ;
    // console.log(control);
    control.removeAt(j);
    }
  }

  getVariants(form: { controls: { variants: { controls: any } } }) {
    return form.controls.variants.controls;
  }

  getColors(form: { controls: { colors: { controls: any } } }) {
    return form.controls.colors.controls;
  }

  addNewVehicleSubmit() {
    console.log(this.vehicle.value);
    this.http
      .post<any>(
        'http://localhost:7001/motordealer/vehicle',
        this.vehicle.value
      )
      .subscribe((data) => {
        console.log(data);
        alert('Vehicle Added');
        window.location.reload();
      });
  }
}
function VehicleInfo(VehicleInfo: any) {
  throw new Error('Function not implemented.');
}
