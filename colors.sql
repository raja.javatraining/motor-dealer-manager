use motor_dealer;

insert into colors values (10001,'Phantom Black');
insert into colors values (10002,'Polar White');
insert into colors values (10003,'Typhoon Silver');
insert into colors values (10004,'Titan Grey');
insert into colors values (10005,'Starry Night');
insert into colors values (10007,'Fiery Red');
insert into colors values (10011,'Phantom Black Roof');
insert into colors values (10012,'Denim Blue');
insert into colors values (10013,'Deep Forest');